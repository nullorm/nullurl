package uris

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/nullorm/handlers"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// Resource handlers for users
type Resource struct {
	collection string
	session    *mgo.Session
}

// Add handler to add a url
func (res *Resource) Add(w http.ResponseWriter, r *http.Request) {
	s := res.session.Copy()
	defer s.Close()

	user := r.Context().Value("user").(*handlers.AdditionalClaims)

	// Unmarshal json
	uri := &URI{}
	err := json.NewDecoder(r.Body).Decode(&uri)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		message, _ := json.Marshal(&Response{Error: err.Error()})
		fmt.Fprintf(w, "%s", message)
		return
	}

	// Validate Incoming Data
	if uri.Long == "" {
		w.WriteHeader(http.StatusPreconditionFailed)
		message, _ := json.Marshal(&Response{Error: "Expected url"})
		fmt.Fprintf(w, "%s", message)
		return
	}

	// Processing

	// DB
	uri.ID = bson.NewObjectId()
	uri.Owner = user.ID
	err = s.DB("").C(res.collection).Insert(uri)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		message, _ := json.Marshal(&Response{Error: err.Error()})
		fmt.Fprintf(w, "%s", message)
		return
	}

	// Marshal json
	u, err := json.Marshal(&Response{Data: uri})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		message, _ := json.Marshal(&Response{Error: err.Error()})
		fmt.Fprintf(w, "%s", message)
	}

	// Send response
	fmt.Fprintf(w, "%s", u)
}

// Delete handler to delete a uri
func (res *Resource) Delete(w http.ResponseWriter, r *http.Request) {
	s := res.session.Copy()
	defer s.Close()

	user := r.Context().Value("user").(*handlers.AdditionalClaims)

	// Unmarshal json
	uri := &URI{}
	err := json.NewDecoder(r.Body).Decode(&uri)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		message, _ := json.Marshal(&Response{Error: err.Error()})
		fmt.Fprintf(w, "%s", message)
		return
	}

	// Validate Incoming Data
	if uri.ID == "" {
		w.WriteHeader(http.StatusPreconditionFailed)
		message, _ := json.Marshal(&Response{Error: err.Error()})
		fmt.Fprintf(w, "%s", message)
		return
	}

	// Processing

	// DB
	query := bson.M{"_id": uri.ID, "owner": user.ID}
	err = s.DB("").C(res.collection).Remove(query)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		message, _ := json.Marshal(&Response{Error: err.Error()})
		fmt.Fprintf(w, "%s", message)
		return
	}

	// Marshal json
	u, err := json.Marshal(&Response{Data: uri})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		message, _ := json.Marshal(&Response{Error: err.Error()})
		fmt.Fprintf(w, "%s", message)
		return
	}

	// Send response
	fmt.Fprintf(w, "%s", u)
}

// FetchAll handler to find all uris belonging to user
func (res *Resource) FetchAll(w http.ResponseWriter, r *http.Request) {
	s := res.session.Copy()
	defer s.Close()

	user := r.Context().Value("user").(*handlers.AdditionalClaims)

	// Processing

	// DB
	var uris []URI
	err := s.DB("").C(res.collection).Find(bson.M{"owner": user.ID}).All(&uris)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		message, _ := json.Marshal(&Response{Error: err.Error()})
		fmt.Fprintf(w, "%s", message)
		return
	}

	// Marshal json
	t, err := json.Marshal(&ResponseMultiple{Data: uris})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		message, _ := json.Marshal(&Response{Error: err.Error()})
		fmt.Fprintf(w, "%s", message)
		return
	}

	// Send response
	fmt.Fprintf(w, "%s", t)
}

// Update handler to update a torrent
func (res *Resource) Update(w http.ResponseWriter, r *http.Request) {
	s := res.session.Copy()
	defer s.Close()

	user := r.Context().Value("user").(*handlers.AdditionalClaims)

	// Unmarshal json
	uri := &URI{}
	err := json.NewDecoder(r.Body).Decode(&uri)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		message, _ := json.Marshal(&Response{Error: err.Error()})
		fmt.Fprintf(w, "%s", message)
		return
	}

	// Validate Incoming Data
	if uri.ID == "" {
		w.WriteHeader(http.StatusPreconditionFailed)
		message, _ := json.Marshal(&Response{Error: "Expected ID"})
		fmt.Fprintf(w, "%s", message)
		return
	}

	// Processing
	// Todo: come up with a better solution
	update := bson.M{}
	if uri.Long != "" {
		update["long"] = uri.Long
	}
	if uri.Name != "" {
		update["name"] = uri.Name
	}
	if len(uri.Tags) != 0 {
		update["tags"] = uri.Tags
	}

	// DB
	query := bson.M{"_id": uri.ID, "owner": user.ID}
	err = s.DB("").C(res.collection).Update(query, bson.M{"$set": update})
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		message, _ := json.Marshal(&Response{Error: err.Error()})
		fmt.Fprintf(w, "%s", message)
		return
	}

	// Marshal json
	u, err := json.Marshal(&Response{Data: uri})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		message, _ := json.Marshal(&Response{Error: err.Error()})
		fmt.Fprintf(w, "%s", message)
		return
	}

	// Send response
	fmt.Fprintf(w, "%s", u)
}

// New initialize user resource with default values
func New(s *mgo.Session) *Resource {
	collection := "uri"
	return &Resource{collection: collection, session: s}
}

package uris

import "gopkg.in/mgo.v2/bson"

// URI url model
type URI struct {
	Long  string        `json:"long"`
	ID    bson.ObjectId `bson:"_id,omitempty" json:"id"`
	Name  string        `bson:"name,omitempty" json:"name"`
	Owner bson.ObjectId `bson:"owner,omitempty" json:"owner"`
	Tags  []string      `json:"tags"`
}

// Response struct
type Response struct {
	Error string `json:"error"`
	Data  *URI   `json:"data"`
}

// ResponseMultiple struct
type ResponseMultiple struct {
	Error string `json:"error"`
	Data  []URI  `json:"data"`
}

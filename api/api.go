package api

import (
	"net/http"

	"gitlab.com/nullorm/nullurl/api/uris"
	"gopkg.in/mgo.v2"
)

// New init api
func New(s *mgo.Session) (mux *http.ServeMux) {
	mux = http.NewServeMux()
	uri := uris.New(s)

	mux.HandleFunc("/add", uri.Add)
	mux.HandleFunc("/delete", uri.Delete)
	mux.HandleFunc("/fetchAll", uri.FetchAll)
	mux.HandleFunc("/update", uri.Update)
	return
}
